# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.
import dash
import dash_bootstrap_components as dbc
from dash import dcc, html
from callbacks import register_callbacks

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.LUX], suppress_callback_exceptions=True,
                meta_tags=[{'name': 'viewport', 'content': 'width=device-width, initial-scale=1'}])
app.config.suppress_callback_exceptions = True

#################################################################################
############################# Layouts ###########################################
#################################################################################

server = app.server

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Nav(id='navbar-container',
             children=[dbc.NavbarSimple(
                 children=[
                     dbc.NavItem(dbc.NavLink("Home", id="home-link", href="/")),
                     dbc.NavItem(dbc.NavLink("Course", id="course-link", href="/course")),
                     dbc.NavItem(
                         dbc.NavLink("Application on explainable AI", id="application-link", href="/application")),
                 ],
                 brand="FX ToolKit",
                 color="primary",
                 dark=True, )]),
    html.Div(id='page-content')
])

#################################################################################
################################# Callback for the app ##########################
#################################################################################
register_callbacks(app)

#################################################################################
################################# Launching app #################################
#################################################################################
if __name__ == '__main__':
    app.run_server(debug=True)
