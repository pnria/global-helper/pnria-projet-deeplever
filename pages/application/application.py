from dash import dcc, html
import dash_bootstrap_components as dbc
import dash_daq as daq


class Model:
    """ Initialisation of the components into the layout"""

    def __init__(self, names_models, dict_components, dic_solvers, dic_xtypes):
        self.dict_components = dict_components
        self.dic_solvers = dic_solvers
        self.dic_xtypes = dic_xtypes

        self.ml_models = names_models
        self.ml_model = None

        self.pretrained_model = None

        self.add_info = False
        self.model_info = None

        self.enum = 1

        self.xtypes = []
        self.xtype = None

        self.solvers = []
        self.solver = None

        self.instance = None

        self.options_expls = {}
        self.options_cont_expls = {}
        self.expl = None
        self.cont_expl = None

        self.component_class = None
        self.component = None


class View:
    """ Creates the layout of the app"""
    def __init__(self, model):
        self.model = model

        self.ml_menu_models = html.Div([
            html.P(),
            html.Label("Choose the Machine Learning algorithm :"),
            html.P(),
            dcc.Dropdown(self.model.ml_models,
                         id='ml_model_choice',
                         className="dropdown")])

        self.pretrained_model_upload = html.Div([
            html.Hr(),
            html.Label("Choose the pretrained model : "),
            html.P(),
            dcc.Upload(
                id='ml_pretrained_model_choice',
                children=html.Div([
                    'Drag and Drop or ',
                    html.A('Select File')
                ]),
                className="upload"
            ),
            html.Div(id='pretrained_model_filename')])

        self.add_model_info_choice = html.Div([
            html.Hr(),
            html.Label("Upload more data model (only for model with categorical variables) ?"),
            html.P(),
            daq.BooleanSwitch(id='add_info_model_choice', on=False, color="#000000", )])

        self.model_info = html.Div(id="choice_info_div",
                                   hidden=True,
                                   children=[
                                       html.Hr(),
                                       html.Label(
                                           "Choose the pretrained model dataset (csv) or feature definition file (txt): "),
                                       html.P(),
                                       dcc.Upload(
                                           id='model_info_choice',
                                           children=html.Div([
                                               'Drag and Drop or ',
                                               html.A('Select File')
                                           ]),
                                           className="upload"
                                       ),
                                       html.Div(id='info_filename')])

        self.instance_upload = html.Div([
            html.Hr(),
            html.Label("Choose the instance to explain : "),
            html.P(),
            dcc.Upload(
                id='ml_instance_choice',
                children=html.Div([
                    'Drag and Drop or ',
                    html.A('Select instance')
                ]),
                className="upload"
            ),
            html.Div(id='instance_filename')])

        self.num_explanation = html.Div([
            html.Label("Choose the number of explanations : "),
            html.P(),
            dcc.Input(
                id="number_explanations",
                value=1,
                type="number",
                placeholder="How many explanations ?",
                className="dropdown"),
            html.Hr()])

        self.type_explanation = html.Div([
            html.Label("Choose the kind of explanation : "),
            html.P(),
            dcc.Checklist(
                id="explanation_type",
                options=self.model.xtypes,
                className="check-boxes",
                inline=True),
            html.Hr()])

        self.solver = html.Div([html.Label("Choose the solver : "),
                                html.P(),
                                dcc.Dropdown(self.model.solvers,
                                             id='solver_sat')])

        self.sidebar = dcc.Tabs(children=[
            dcc.Tab(label='Basic Parameters', children=[
                self.ml_menu_models,
                self.add_model_info_choice,
                self.model_info,
                dcc.Store(id='intermediate-value-data'),
                self.pretrained_model_upload,
                html.P(),
                html.Button('Submit model', id='submit-model', n_clicks=0, className="button"),
                html.Hr(),
                self.instance_upload,
                html.Hr(),
                html.Label("Set Advanced parameters then submit :"),
                html.P(),
                html.Button('Submit for explanation', id='submit-instance', n_clicks=0, className="button"), ],
                    className="sidebar"),
            dcc.Tab(label='Advanced Parameters', children=[
                html.P(),
                self.num_explanation,
                self.type_explanation,
                self.solver
            ], className="sidebar")])

        self.switch = html.Div(id="div_switcher_draw_expl", hidden=True,
                               children=[html.Label("Draw explanations ?"),
                                         html.P(),
                                         daq.BooleanSwitch(id='drawing_expl', on=False, color="#FFFFFF", )])

        self.expl_choice = html.Div(id="interaction_graph", hidden=True,
                                    children=[html.H5("Navigate through the explanations and plot them on the tree : "),
                                              html.Div(children=[dcc.Dropdown(self.model.options_expls,
                                                                              id='expl_choice',
                                                                              className="dropdown")]),
                                              html.H5(
                                                  "Navigate through the contrastive explanations and plot them on the tree : "),
                                              html.Div(children=[dcc.Dropdown(self.model.options_cont_expls,
                                                                              id='cont_expl_choice',
                                                                              className="dropdown")])])

        self.tree_to_plot = html.Div(id="choosing_tree", hidden=True,
                                     children=[html.H5("Choose a tree to plot: "),
                                               html.Div(children=[dcc.Slider(0, 20, 1, marks=None,
                                                                 tooltip={"placement": "bottom", "always_visible": True},
                                                                 value=0,
                                                                 id='choice_tree')])])

        self.layout = dbc.Row([dbc.Col([self.sidebar],
                                       width=3, class_name="sidebar"),
                               dbc.Col([dbc.Row(id="graph", children=[]),
                                        dbc.Row(self.switch),
                                        dbc.Row(self.expl_choice),
                                        dbc.Row(self.tree_to_plot)],
                                       width=5, class_name="column_graph"),
                               dbc.Col(html.Main(id="explanation", children=[], hidden=True), width=4)])
