#!/usr/bin/env python
#-*- coding:utf-8 -*-
##
## tree.py (reuses parts of the code of SHAP)
##
##  Created on: Dec 7, 2018
##      Author: Nina Narodytska
##      E-mail: narodytska@vmware.com
##

#
#==============================================================================
from anytree import Node, RenderTree,AsciiStyle
import json
import numpy as np
import math
import six


#
#==============================================================================
class xgnode(Node):
    def __init__(self, id, parent = None):
        Node.__init__(self, id, parent)
        self.id = id  # node value
        self.name = None
        self.left_node_id = -1   # left child
        self.right_node_id = -1  # right child

        self.feature = -1
        self.threshold = None
        self.values = -1 
        #iai
        self.split = None

    def __str__(self):
        pref = '   ' * self.depth 
        if (len(self.children) == 0):
            return (pref+ f"leaf:{self.id}  {self.values}")
        else:
            if(self.name is None):
                if (self.threshold is None):
                    return (pref+ f"({self.id}) f{self.feature}")
                else:
                    return (pref+ f"({self.id}) f{self.feature} = {self.threshold}")                    
            else:
                if (self.threshold is None):
                    return (pref+ f"({self.id}) \"{self.name}\"")
                else:
                    return (pref+ f"({self.id}) \"{self.name}\" = {self.threshold}")

#
#==============================================================================

def walk_tree(node):
    if (len(node.children) == 0):
        # leaf
        print(node)
    else:
        print(node)
        walk_tree(node.children[0])
        walk_tree(node.children[1])


#
#==============================================================================
def scores_tree(node, sample):
    if (len(node.children) == 0):
        # leaf
        return node.values
    else:
        feature_branch = node.feature
        sample_value = sample[feature_branch]
        assert(sample_value is not None)
        if(sample_value < node.threshold):
            return scores_tree(node.children[0], sample)
        else:
            return scores_tree(node.children[1], sample)



#
#==============================================================================
def get_json_tree(model, tool, maxdepth=None):
    """
        returns the dtree in JSON format 
    """
    jt = None
    if tool == "DL85":
        jt = model.tree_
    elif tool == "IAI":
        fname = os.path.splitext(os.path.basename(fname))[0]
        dir_name = os.path.join("temp", f"{tool}{maxdepth}") 
        try:
            os.stat(dir_name)
        except:
            os.makedirs(dir_name)      
        iai_json = os.path.join(dir_name, fname+'.json')    
        model.write_json(iai_json) 
        print(f'load JSON tree from {iai_json} ...')
        with open(iai_json) as fp:
            jt = json.load(fp)
    elif tool == "ITI":
        print(f'load JSON tree from {model.json_name} ...')
        with open(model.json_name) as fp:
            jt = json.load(fp)
    #else:
    #    assert False, 'Unhandled model type: {0}'.format(self.tool) 
            
    return jt

#
#==============================================================================
class UploadedDecisionTree:
    """ A decision tree.
    This object provides a common interface to many different types of models.
    """
    def __init__(self, model, tool, maxdepth, feature_names=None, nb_classes = 0):
        self.tool  = tool
        self.model = model
        self.tree  = None
        self.depth = None
        self.n_nodes = None
        json_tree = get_json_tree(self.model, self.tool, maxdepth)
        self.tree, self.n_nodes, self.depth = self.build_tree(json_tree, feature_names)
             
    def print_tree(self):
        print("DT model:")
        walk_tree(self.tree)


    def dump(self, fvmap, filename=None, maxdepth=None, feat_names=None): 
        """
            save the dtree and data map in .dt/.map file   
        """
                     
        def walk_tree(node, domains, internal, terminal):
            """
                extract internal (non-term) & terminal nodes
            """
            if (len(node.children) == 0): # leaf node
                terminal.append((node.id, node.values))
            else:
                assert (node.children[0].id == node.left_node_id)
                assert (node.children[1].id == node.right_node_id)
                
                f = f"f{node.feature}"
                
                if self.tool == "DL85":
                    l,r = (1,0)
                    internal.append((node.id, f, l, node.children[0].id))
                    internal.append((node.id, f, r, node.children[1].id))
                    
                elif self.tool == "ITI":
                    #l,r = (0,1)                    
                    if len(fvmap[f]) > 2:
                        n = 0
                        for v in fvmap[f]:
                            if (fvmap[f][v][2] == node.threshold) and \
                                            (fvmap[f][v][1] == True):
                                l = v
                                n = n + 1
                            if (fvmap[f][v][2] == node.threshold) and \
                                            (fvmap[f][v][1] == False):  
                                r = v
                                n = n + 1
                                
                        assert (n == 2)      
                            
                    elif (fvmap[f][0][2] == node.threshold):
                        l,r = (0,1)
                    else:
                        assert (fvmap[f][1][2] == node.threshold)
                        l,r = (1,0)
      
                    internal.append((node.id, f, l, node.children[0].id))
                    internal.append((node.id, f, r, node.children[1].id))                            
                        
                elif self.tool == "IAI":
                    left, right = [], []
                    for p in fvmap[f]:
                        if fvmap[f][p][1] == True:
                            assert (fvmap[f][p][2] in node.split)
                            if node.split[fvmap[f][p][2]]: 
                                left.append(p)
                            else:
                                right.append(p)
                    
                    internal.extend([(node.id, f, l, node.children[0].id) for l in left]) 
                    internal.extend([(node.id, f, r, node.children[1].id) for r in right])    
                
                elif self.tool == 'SKL':
                    left, right = [], []
                    for j in domains[f]:
                        if np.float32(fvmap[f][j][2]) <= np.float32(node.threshold):
                            left.append(j)
                        else:
                            right.append(j)

                    internal.extend([(node.id, f, l, node.children[0].id) for l in left]) 
                    internal.extend([(node.id, f, r, node.children[1].id) for r in right]) 
                    
                    dom0, dom1 = dict(), dict()
                    dom0.update(domains)
                    dom1.update(domains)
                    dom0[f] = left
                    dom1[f] = right                     
                  
                else:
                    assert False, 'Unhandled model type: {0}'.format(self.tool)

                
                internal, terminal = walk_tree(node.children[0], dom0, internal, terminal)
                internal, terminal = walk_tree(node.children[1], dom1, internal, terminal)
                
            return internal, terminal 
        
        domains = {f:[j for j in fvmap[f] if((fvmap[f][j][1]))] for f in fvmap}
        internal, terminal = walk_tree(self.tree, domains, [], [])
        

        dt = f"{self.n_nodes}\n{self.tree.id}\n"
        dt += f"I {' '.join(dict.fromkeys([str(i) for i,_,_,_ in internal]))}\n"
        dt +=f"T {' '.join([str(i) for i,_ in terminal ])}\n"
        for i,c in terminal:
            dt +=f"{i} T {c}\n"            
        for i,f, j, n in internal: 
            dt +=f"{i} {f} {j} {n}\n"

        map = "Categorical\n"
        map += f"{len(fvmap)}"
        for f in fvmap:
            for v in fvmap[f]:
                if (fvmap[f][v][1] == True):
                    map += f"\n{f} {v} ={fvmap[f][v][2]}"
                if (fvmap[f][v][1] == False) and self.tool == "ITI":
                    map += f"\n{f} {v} !={fvmap[f][v][2]}"
    
            
        if feat_names is not None:
            features_names_mapping = []
            for i,fid in enumerate(feat_names):
                feat=f'f{i}'
                f = f'T:C,{fid}:{feat},'+",".join([f'{fvmap[feat][v][2]}:{v}' for v in fvmap[feat] if(fvmap[feat][v][1])])
                features_names_mapping.append(f)

        return dt, map, features_names_mapping    

    def convert_dt(self, feat_names):
        """
            save dtree in .dt format & generate dtree map from the tree
        """        
        def walk_tree(node, domains, internal, terminal):
            """
                extract internal (non-term) & terminal nodes
            """
            if not len(node.children): # leaf node
                terminal.append((node.id, node.values))
            else:
                # internal node
                f = f"f{node.feature}"    
                left, right = [], []
                for j in domains[f]:
                    if self.intvs[f][j] <= node.threshold:
                        left.append(j)
                    else:
                        right.append(j)

                internal.extend([(node.id, f, l, node.children[0].id) for l in left]) 
                internal.extend([(node.id, f, r, node.children[1].id) for r in right]) 
                    
                dom0, dom1 = dict(), dict()
                dom0.update(domains)
                dom1.update(domains)
                dom0[f] = left
                dom1[f] = right                     
                #
                internal, terminal = walk_tree(node.children[0], dom0, internal, terminal)
                internal, terminal = walk_tree(node.children[1], dom1, internal, terminal)
                
            return internal, terminal 
        
        assert (self.tool == 'SKL')
        domains = {f:[j for j in range(len(self.intvs[f]))] for f in self.intvs}
        internal, terminal = walk_tree(self.tree, domains, [], []) 
        
        dt = f"{self.n_nodes}\n{self.tree.id}\n"
        dt += f"I {' '.join(dict.fromkeys([str(i) for i,_,_,_ in internal]))}\n"
        dt += f"T {' '.join([str(i) for i,_ in terminal ])}"
        for i,c in terminal:
            dt += f"\n{i} T {c}"           
        for i,f, j, n in internal: 
            dt += f"\n{i} {f} {j} {n}"
                
        map = "Ordinal\n"  
        map += f"{len(self.intvs)}"
        for f in self.intvs:
            for j,t in enumerate(self.intvs[f][:-1]):
                map += f"\n{f} {j} <={np.round(float(t),4)}"
            map += f"\n{f} {j+1} >{np.round(float(t),4)}"  


        if feat_names is not None:
            features_names_mapping = []
            for i,fid in enumerate(feat_names):
                feat=f'f{i}'
                if feat in self.intvs:
                    f = f'T:O,{fid}:{feat},'
                    f += ",".join([f'{t}:{j}' for j,t in enumerate(self.intvs[feat])])                    
                    features_names_mapping.append(f)

        return dt, map, features_names_mapping    
    
    
    def build_tree(self, json_tree=None, feature_names=None):
    
        def extract_data(json_node, idx, depth=0, root=None, feature_names=None):
            """
                Incremental Tree Inducer / DL8.5
            """
            if (root is None):
                node = xgnode(idx)
            else:
                node = xgnode(idx, parent = root)
                
            if "feat" in json_node:                
                
                if self.tool == "ITI": #f0, f1, ...,fn
                    node.feature = json_node["feat"][1:]
                else:
                    node.feature = json_node["feat"] #json DL8.5
                if (feature_names is not None):
                    node.name = feature_names[node.feature] 
                
                if self.tool == "ITI":
                    node.threshold = json_node[json_node["feat"]]
                    
                node.left_node_id = idx + 1     
                _, idx, d1 = extract_data(json_node['left'], idx+1, depth+1, node, feature_names)  
                node.right_node_id = idx + 1            
                _, idx, d2 = extract_data(json_node['right'], idx+1, depth+1, node, feature_names)
                depth = max(d1, d2)
               
            elif "value" in json_node:
                node.values  = json_node["value"]
                
            return node, idx, depth
        
        
        def extract_iai(lnr, json_tree, feature_names = None):
            """
                Interpretable AI tree
            """
            
            json_tree = json_tree['tree_']
            nodes = []
            depth = 0
            for i, json_node in enumerate(json_tree["nodes"]):
                if json_node["parent"] == -2:
                    node = xgnode(json_node["id"])
                else:
                    root = nodes[json_node["parent"] - 1]
                    node = xgnode(json_node["id"], parent = root)
                    
                    assert (json_node["parent"] > 0)
                    assert (root.id == json_node["parent"])

                if json_node["split_type"] == "LEAF":
                    #node.values = target[json_node["fit"]["class"] - 1]
                    ##assert json_node["fit"]["probs"][node.values] == 1.0
                    node.values = lnr.get_classification_label(node.id)   
                    depth = max(depth, lnr.get_depth(node.id))
                    
                    assert (json_node["lower_child"] == -2 and json_node["upper_child"] == -2)
                    
                elif json_node["split_type"] == "MIXED":   
                    #node.feature = json_node["split_mixed"]["categoric_split"]["feature"] - 1
                    #node.left_node_id  = json_node["lower_child"]
                    #node.right_node_id = json_node["upper_child"]
                    
                    node.feature = lnr.get_split_feature(node.id)
                    node.left_node_id  = lnr.get_lower_child(node.id)
                    node.right_node_id = lnr.get_upper_child(node.id)
                    node.split = lnr.get_split_categories(node.id)

                    
                    assert (json_node["split_mixed"]["categoric_split"]["feature"] > 0)
                    assert (json_node["lower_child"] > 0)
                    assert (json_node["upper_child"] > 0)
                    
                else:
                    assert False, 'Split feature is not \"categoric_split\"' 
                        
                nodes.append(node)
                
            return nodes[0], json_tree["node_count"], depth
        
        
        def extract_skl(tree_, classes_, feature_names=None):
            """
                scikit-learn tree
            """
    
            def get_CART_tree(tree_):
                n_nodes = tree_.node_count
                children_left = tree_.children_left
                children_right = tree_.children_right
                #feature = tree_.feature
                #threshold = tree_.threshold
                #values = tree_.value
                node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    
                is_leaf = np.zeros(shape=n_nodes, dtype=bool)
                stack = [(0, -1)]  # seed is the root node id and its parent depth
                while len(stack) > 0:
                    node_id, parent_depth = stack.pop()
                    node_depth[node_id] = parent_depth + 1
    
                    # If we have a test node
                    if (children_left[node_id] != children_right[node_id]):
                        stack.append((children_left[node_id], parent_depth + 1))
                        stack.append((children_right[node_id], parent_depth + 1))
                    else:
                        is_leaf[node_id] = True

                return children_left, children_right, is_leaf, node_depth    
    
            children_left, children_right, is_leaf, node_depth = get_CART_tree(tree_)
            feature = tree_.feature
            threshold = tree_.threshold
            values = tree_.value
            m = tree_.node_count  
            assert (m > 0), "Empty tree"
            ##
            self.intvs = {f'f{feature[i]}':set([]) for i in range(tree_.node_count) if not is_leaf[i]}
            for i in range(tree_.node_count):
                if not is_leaf[i]:
                    self.intvs[f'f{feature[i]}'].add(threshold[i])
            self.intvs = {f: sorted(self.intvs[f])+[math.inf] for f in six.iterkeys(self.intvs)}

            def extract_data(idx, root = None, feature_names = None):
                i = idx
                assert (i < m), "Error index node"
                if (root is None):
                    node = xgnode(i)
                else:
                    node = xgnode(i, parent = root)
                if is_leaf[i]:
                    node.values = classes_[np.argmax(values[i])]
                else:
                    node.feature = feature[i]
                    if (feature_names):
                        node.name = feature_names[feature[i]]
                    node.threshold = threshold[i]
                    node.left_node_id = children_left[i]
                    node.right_node_id = children_right[i]
                    extract_data(node.left_node_id, node, feature_names)
                    extract_data(node.right_node_id, node, feature_names)            
                    
                return node
    
            root = extract_data(0, None, feature_names)
            return root, tree_.node_count, tree_.max_depth

        
        
        root, node_count, maxdepth = None, None, None
        
        if(self.tool == 'SKL'):
            if "feature_names_in_" in dir(self.model):
                feature_names = self.model.feature_names_in_
            root, node_count, maxdepth = extract_skl(self.model.tree_, self.model.classes_, feature_names)
            
        if json_tree:
            if self.tool == "IAI":
                root, node_count, maxdepth = extract_iai(self.model, json_tree, feature_names)
            else:        
                root,_,maxdepth = extract_data(json_tree, 1, 0, None, feature_names)
                node_count = json.dumps(json_tree).count('feat') + json.dumps(json_tree).count('value')            
            
        return root, node_count, maxdepth    