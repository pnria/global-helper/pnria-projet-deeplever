#!/usr/bin/env python
# -*- coding:utf-8 -*-
##
## dtviz.py
##
##  Created on: Jul 7, 2020
##      Author: Alexey Ignatiev
##      E-mail: alexey.ignatiev@monash.edu
##

#
# ==============================================================================
import getopt
import pydot
import ast
import re


#
# ==============================================================================
def create_legend(G):
    legend = pydot.Cluster('legend', rankdir="TB")

    # non-terminal nodes
    for n in ["a", "b", "c", "d", "e", "f"]:
        node = pydot.Node(n, label=n)
        legend.add_node(node)

    edge = pydot.Edge("a", "b")
    edge.obj_dict['attributes']["label"] = "instance"
    edge.obj_dict['attributes']["style"] = "dashed"
    legend.add_edge(edge)

    edge = pydot.Edge("e", "f")
    edge.obj_dict['attributes']["label"] = "contrastive \n explanation"
    edge.obj_dict['attributes']["color"] = "red"
    edge.obj_dict['attributes']["style"] = "dashed"
    legend.add_edge(edge)

    edge = pydot.Edge("c", "d")
    edge.obj_dict['attributes']["label"] = "instance with \n explanation"
    edge.obj_dict['attributes']["color"] = "blue"
    edge.obj_dict['attributes']["style"] = "dashed"
    legend.add_edge(edge)

    G.add_subgraph(legend)


#
# ==============================================================================
def visualize(dt):
    """
        Visualize a DT with graphviz.
    """

    G = pydot.Dot('tree_total', graph_type='graph')

    g = pydot.Cluster('tree', graph_type='graph')

    # non-terminal nodes
    for n in dt.nodes:
        node = pydot.Node(n, label=dt.feature_names[dt.nodes[n].feat], shape="circle")
        g.add_node(node)

    # terminal nodes
    for n in dt.terms:
        node = pydot.Node(n, label=dt.terms[n], shape="square")
        g.add_node(node)

    # transitions
    for n1 in dt.nodes:
        for v in dt.nodes[n1].vals:
            n2 = dt.nodes[n1].vals[v]
            edge = pydot.Edge(n1, n2)
            if len(v) == 1:
                edge.obj_dict['attributes']['label'] = dt.fvmap[tuple([dt.nodes[n1].feat, tuple(v)[0]])]
            else:
                edge.obj_dict['attributes']['label'] = '{0}'.format(
                    '\n'.join([dt.fvmap[tuple([dt.nodes[n1].feat, val])] for val in tuple(v)]))
            edge.obj_dict['attributes']['fontsize'] = 10
            edge.obj_dict['attributes']['arrowsize'] = 0.8

            g.add_edge(edge)

    G.add_subgraph(g)

    return G.to_string()


#
# ==============================================================================
def visualize_instance(dt, instance):
    """
        Visualize a DT with graphviz and plot the running instance.
    """
    # path that follows the instance - colored in blue
    path, term, depth = dt.execute(instance)
    edges_instance = []
    for i in range(len(path) - 1):
        edges_instance.append((path[i], path[i + 1]))
    edges_instance.append((path[-1], "term:" + term))

    G = pydot.Dot('tree_total', graph_type='graph')

    g = pydot.Cluster('tree', graph_type='graph')

    # non-terminal nodes
    for n in dt.nodes:
        node = pydot.Node(n, label=dt.feature_names[dt.nodes[n].feat], shape="circle")
        g.add_node(node)

    # terminal nodes
    for n in dt.terms:
        node = pydot.Node(n, label=dt.terms[n], shape="square")
        g.add_node(node)

    # transitions
    for n1 in dt.nodes:
        for v in dt.nodes[n1].vals:
            n2 = dt.nodes[n1].vals[v]
            n2_type = g.get_node(str(n2))[0].obj_dict['attributes']['shape']
            edge = pydot.Edge(n1, n2)
            if len(v) == 1:
                edge.obj_dict['attributes']['label'] = dt.fvmap[tuple([dt.nodes[n1].feat, tuple(v)[0]])]
            else:
                edge.obj_dict['attributes']['label'] = '{0}'.format(
                    '\n'.join([dt.fvmap[tuple([dt.nodes[n1].feat, val])] for val in tuple(v)]))
            edge.obj_dict['attributes']['fontsize'] = 10
            edge.obj_dict['attributes']['arrowsize'] = 0.8

            # instance path in dashed
            if ((n1, n2) in edges_instance) or (n2_type == 'square' and (n1, "term:" + dt.terms[n2]) in edges_instance):
                edge.obj_dict['attributes']['style'] = 'dashed'

            g.add_edge(edge)

    create_legend(G)
    G.add_subgraph(g)

    return G.to_string()


# ==============================================================================
def visualize_expl(dt, instance, expl):
    """
        Visualize a DT with graphviz and plot the running instance.
    """
    # path that follows the instance - colored in blue
    path, term, depth = dt.execute(instance)
    edges_instance = []
    for i in range(len(path) - 1):
        edges_instance.append((path[i], path[i + 1]))
    edges_instance.append((path[-1], "term:" + term))

    g = pydot.Dot('my_graph', graph_type='graph')

    # non-terminal nodes
    for n in dt.nodes:
        node = pydot.Node(n, label=dt.feature_names[dt.nodes[n].feat], shape="circle")
        g.add_node(node)

    # terminal nodes
    for n in dt.terms:
        node = pydot.Node(n, label=dt.terms[n], shape="square")
        g.add_node(node)

    # transitions
    for n1 in dt.nodes:
        for v in dt.nodes[n1].vals:
            n2 = dt.nodes[n1].vals[v]
            n2_type = g.get_node(str(n2))[0].obj_dict['attributes']['shape']
            edge = pydot.Edge(n1, n2)
            if len(v) == 1:
                edge.obj_dict['attributes']['label'] = dt.fvmap[tuple([dt.nodes[n1].feat, tuple(v)[0]])]
            else:
                edge.obj_dict['attributes']['label'] = '{0}'.format(
                    '\n'.join([dt.fvmap[tuple([dt.nodes[n1].feat, val])] for val in tuple(v)]))
            edge.obj_dict['attributes']['fontsize'] = 10
            edge.obj_dict['attributes']['arrowsize'] = 0.8

            # instance path in dashed
            if ((n1, n2) in edges_instance) or (n2_type == 'square' and (n1, "term:" + dt.terms[n2]) in edges_instance):
                edge.obj_dict['attributes']['style'] = 'dashed'

            for label in edge.obj_dict['attributes']['label'].split('\n'):
                if label in expl:
                    edge.obj_dict['attributes']['color'] = 'blue'

            g.add_edge(edge)

    return g.to_string()


# ==============================================================================
def visualize_contrastive_expl(dt, instance, cont_expl):
    """
        Visualize a DT with graphviz and plot the running instance.
    """
    # path that follows the instance - colored in blue
    path, term, depth = dt.execute(instance)
    edges_instance = []
    for i in range(len(path) - 1):
        edges_instance.append((path[i], path[i + 1]))
    edges_instance.append((path[-1], "term:" + term))

    g = pydot.Dot('my_graph', graph_type='graph')

    # non-terminal nodes
    for n in dt.nodes:
        node = pydot.Node(n, label=dt.feature_names[dt.nodes[n].feat], shape="circle")
        g.add_node(node)

    # terminal nodes
    for n in dt.terms:
        node = pydot.Node(n, label=dt.terms[n], shape="square")
        g.add_node(node)

    # transitions
    for n1 in dt.nodes:
        for v in dt.nodes[n1].vals:
            n2 = dt.nodes[n1].vals[v]
            n2_type = g.get_node(str(n2))[0].obj_dict['attributes']['shape']
            edge = pydot.Edge(n1, n2)
            if len(v) == 1:
                edge.obj_dict['attributes']['label'] = dt.fvmap[tuple([dt.nodes[n1].feat, tuple(v)[0]])]
            else:
                edge.obj_dict['attributes']['label'] = '{0}'.format(
                    '\n'.join([dt.fvmap[tuple([dt.nodes[n1].feat, val])] for val in tuple(v)]))
            edge.obj_dict['attributes']['fontsize'] = 10
            edge.obj_dict['attributes']['arrowsize'] = 0.8

            # instance path in dashed
            if ((n1, n2) in edges_instance) or (n2_type == 'square' and (n1, "term:" + dt.terms[n2]) in edges_instance):
                edge.obj_dict['attributes']['style'] = 'dashed'

            for label in edge.obj_dict['attributes']['label'].split('\n'):
                if label in cont_expl:
                    edge.obj_dict['attributes']['color'] = 'red'

            g.add_edge(edge)

    return g.to_string()
