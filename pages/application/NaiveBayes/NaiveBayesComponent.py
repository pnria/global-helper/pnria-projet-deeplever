from os import path
import base64 

import dash_bootstrap_components as dbc
import numpy as np
from dash import dcc, html
import subprocess
import shlex



class NaiveBayesComponent():

    def __init__(self, model, info=None, type_info=''):
        
        #Conversion model
        p=subprocess.Popen(['perl','pages/application/NaiveBayes/utils_old/cnbc2xlc.pl', model],stdout=subprocess.PIPE)
        print(p.stdout.read())

        self.naive_bayes = model
        self.map_file = ""

        self.network = html.Div([])
        self.explanation = []


    def update_with_explicability(self, instance, enum, xtype, solver) :

        # Call explanation
        p=subprocess.Popen(['perl','pages/application/NaiveBayes/utils_old/xpxlc.pl', self.naive_bayes, instance, self.map_file],stdout=subprocess.PIPE)
        print(p.stdout.read())
       
        self.explanation = []
        list_explanations_path=[]
        explanation = {}

        self.network = html.Div([])

        #Creating a clean and nice text component
        #instance plotting
        self.explanation.append(html.H4("Instance : \n"))
        self.explanation.append(html.P(str([str(instance[i]) for i in range (len(instance))])))
        for k in explanation.keys() :
            if k != "List of path explanation(s)" and k!= "List of path contrastive explanation(s)" :
                if k in ["List of abductive explanation(s)","List of contrastive explanation(s)"] :
                    self.explanation.append(html.H4(k))
                    for expl in explanation[k] :
                        self.explanation.append(html.Hr())
                        self.explanation.append(html.P(expl))
                        self.explanation.append(html.Hr())
                else :
                    self.explanation.append(html.P(k + explanation[k]))
            else :
                list_explanations_path = explanation["List of path explanation(s)"]
                list_contrastive_explanations_path = explanation["List of path contrastive explanation(s)"]

        return list_explanations_path, list_contrastive_explanations_path
