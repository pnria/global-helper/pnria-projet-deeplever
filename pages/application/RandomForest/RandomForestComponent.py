import re

import numpy
from dash import html
import dash_interactive_graphviz
from sklearn import tree
from pages.application.RandomForest.utils import xrf
from pages.application.RandomForest.utils.xrf.xforest import XRF, Dataset
from sklearn.ensemble._voting import VotingClassifier
from sklearn.ensemble import RandomForestClassifier


class RandomForestComponent:
    """ The component for Random Forest models"""

    def __init__(self, model, info=None, type_info=''):

        # Conversion model
        self.data = Dataset(info)

        # creation of model
        if info is not None and 'csv' in type_info:
            if isinstance(model, xrf.rndmforest.RF2001):
                self.random_forest = XRF(model, self.data.feature_names, self.data.target_name)
            elif isinstance(model, RandomForestClassifier):
                params = {'n_trees': model.n_estimators,
                          'depth': model.max_depth}
                cls = xrf.rndmforest.RF2001(**params)
                train_accuracy, test_accuracy = cls.train(self.data)
                self.random_forest = XRF(cls, self.data.feature_names, self.data.target_name)
            # encoding here so not in the explanation ?

        # visual
        self.tree_to_plot = 0
        dot_source = tree.export_graphviz(self.random_forest.cls.estimators()[self.tree_to_plot],
                                          feature_names=self.data.feature_names, class_names=list(map(lambda cl : str(cl), self.data.target_name)),
                                          impurity=False, filled=True, rounded=True)
        dot_source = re.sub(r"(samples) \= [0-9]+", '', dot_source)
        dot_source = re.sub(r"\\nvalue \= \[[\d+|\,|\s]+\]\\n", '', dot_source)
        dot_source = re.sub(r"\\nclass \= \d+", '', dot_source)

        self.network = html.Div([dash_interactive_graphviz.DashInteractiveGraphviz(
            dot_source=dot_source, style={"width": "50%",
                                          "height": "80%",
                                          "background-color": "transparent"}
        )])

        # init explanation
        self.explanation = []
        self.options_cont_expls = {}
        self.options_expls = {}

    def update_with_explicability(self, instances, enum_feats=None, xtypes=None, solver=None):
        r""" Called when an instance is upload or when you press the button "Submit for explanation" with advanced parameters.
        Args:
            instances : list - list of instance to explain
            enum_feats : ghost feature
            xtypes : types of explanation
            solver : solver, only SAT available for the moment
        """
        instances = [list(map(lambda feature: feature[1], instance)) for instance in instances]
        self.explanation = []
        for instance in instances:
            self.explanation.append(html.H4("Sample : "))
            # Call instance
            self.explanation.append(html.H5("Instance"))
            self.explanation.append(html.Hr())
            self.explanation.append(
                html.P(str([tuple((self.data.feature_names[i], str(instance[i]))) for i in
                            range(len(instance) - 1)]) + " THEN " + str(
                    tuple((self.data.target_name, str(instance[-1]))))))
            self.explanation.append(html.Hr())

            # Call explanation
            xtypes_trad = {"abd":  " Abductive ", "con" :  "Contrastive "}
            for xtype in xtypes :
                explanation_result = self.random_forest.explain(instance, xtype)
                # Creating a clean and nice text component
                for k in explanation_result.keys():
                    self.explanation.append(html.H5(xtypes_trad[xtype] + k))
                    self.explanation.append(html.Hr())
                    self.explanation.append(html.P(explanation_result[k]))
                    self.explanation.append(html.Hr())

            del self.random_forest.enc
            del self.random_forest.x

        return [], []

    def update_plotted_tree(self, tree_to_plot):
        r""" Called by a slider to choose which tree in a random forest to plot
        Args:
            tree_to_plot : int - the id of the tree to plot
        """
        # visual
        self.tree_to_plot = tree_to_plot
        dot_source = tree.export_graphviz(self.random_forest.cls.estimators()[self.tree_to_plot],
                                          feature_names=self.data.feature_names, class_names=list(map(lambda cl : str(cl), self.data.target_name)),
                                          impurity=False, filled=True, rounded=True)
        dot_source = re.sub(r"(samples) \= [0-9]+", '', dot_source)
        dot_source = re.sub(r"\\nvalue \= \[[\d+|\,|\s]+\]\\n", '', dot_source)
        dot_source = re.sub(r"\\nclass \= \d+", '', dot_source)

        self.network = html.Div([dash_interactive_graphviz.DashInteractiveGraphviz(
            dot_source=dot_source, style={"width": "50%",
                                          "height": "80%",
                                          "background-color": "transparent"}
        )])
