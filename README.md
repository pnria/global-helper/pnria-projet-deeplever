# FXTool Kit by PNRIA

## About explicability

The project consists in deploying the explicability algorithms from Joao Marques-Silva’s team at ANITI on a web platform.

Indeed they developed multiple algorithms to explain Decision Tree, Random Forest, Naive Bayes, Neural Network … But these algorithms are not accessible to other researchers so the team wants a “showcase site” .
The tools are described in recent papers by ANITI’s DeepLever Chair, with prototypes already available for most of the papers.

Please visit Joao Marques-Silva's work on : [Github Repo](https://github.com/jpmarquessilva/) 
You will see some repos integrated here.

## Requirements 

Python-3.8.10

```commandline
$ pip install dash
$ pip install pandas
$ pip install -r requirements.txt
```

Import graphviz manually

## Structure

The structure is adapted for deployment on Heroku, if you don't wish to deploy, you can delete Procfile and runtime.txt.


## Running locally

Run app.py then visit localhost

Set the parameters and upload the models (you should upload data for random forest).
The instance should either be a .txt at format (feature1=...,feature2=...) or a json file

## Deployed app

Visit :
[App web](https://aniti-fxtools.herokuapp.com/)

Set the parameters and upload the models (you should upload data for random forest).
The instance should either be a .txt at format (feature1=...,feature2=...) or a json file

To deploy new changes, please execute this code : 
```commandline
$ git status # view the changes
$ git add .  # add all the changes
$ git commit -m 'a description of the changes'
$ git push heroku master
```

However, for this free deployment, you have a limited slug size. It might need to be deployed somewhere else.

## Authors

CNRS-IRIT collaboration with ANITI for PNRIA

[de Pourtales Caroline](https://www.linkedin.com/in/caroline-de-pourtales/)