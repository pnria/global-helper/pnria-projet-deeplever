import base64
import io
import json

import joblib
import pickle
from dash import html

from pages.application.RandomForest.utils import xrf
from pages.application.RandomForest.utils.xrf import *
sys.modules['xrf'] = xrf


def parse_contents_graph(contents, filename):
    r"""
    Extract content from model file

    Args:
        contents : the contents of the model file
        filename : the name of the model file

    Return:
        Content of model file
    """
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if '.pkl' in filename:
            try:
                data = joblib.load(io.BytesIO(decoded))
            except:
                data = pickle.load(io.BytesIO(decoded))
        elif '.txt' in filename:
            data = decoded.decode('utf-8').strip()
        elif '.dt' in filename:
            data = decoded.decode('utf-8').strip()
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])

    return data


def parse_contents_data(contents, filename):
    r"""
    Extract content from data file

    Args:
        contents : the contents of the data file
        filename : the name of the data file

    Return:
        Content of data file
    """
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if '.csv' in filename:
            data = decoded.decode('utf-8').strip()
        if '.txt' in filename:
            data = decoded.decode('utf-8').strip()
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])

    return data


def split_instance_according_to_format(instance, features_names=None):
    r"""
    Format instance
    """
    if "=" in instance:
        splitted_instance = [tuple((v.split('=')[0].strip(), float(v.split('=')[1].strip()))) for v in
                             instance.split(',')]
    else:
        if features_names:
            splitted_instance = [tuple((features_names[i], float(instance.split(',')[i].strip()))) for i in
                                 range(len(instance.split(',')))]
        else:
            splitted_instance = [tuple(("feature_{0}".format(i), float(instance.split(',')[i].strip()))) for i in
                                 range(len(instance.split(',')))]

    return splitted_instance


def parse_contents_instance(contents, filename):
    r"""
    Extract content from instance file

    Args:
        contents : the contents of the instance file
        filename : the name of the instance file

    Return:
        Content of instance file
    """
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if '.csv' in filename:
            data = decoded.decode('utf-8')
            features_names = str(data).strip().split('\n')[0]
            features_names = str(features_names).strip().split(',')
            data = str(data).strip().split('\n')[1:]
            data = list(map(lambda inst: split_instance_according_to_format(inst, features_names), data))
        elif '.txt' in filename:
            data = decoded.decode('utf-8')
            data = str(data).split('\n')
            data = list(map(lambda inst: split_instance_according_to_format(inst), data))
        elif '.json' in filename:
            data = decoded.decode('utf-8').strip()
            data = json.loads(data)
            data = [tuple(data.items())]
        elif '.inst' in filename:
            data = decoded.decode('utf-8').strip()
            data = json.loads(data)
            data = [tuple(data.items())]
        elif '.samples' in filename:
            decoded = decoded.decode('utf-8').strip()
            data = str(decoded).split('\n')
            data = list(map(lambda inst: split_instance_according_to_format(inst), data))
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])

    return data


def extract_data(data):
    r"""
    Args:
        data : json containing information about the models available for explicability
    """
    names_models = [data[i]['ml_type'] for i in range(len(data))]
    dict_components = {}
    dic_solvers = {}
    dic_xtypes = {}

    for i in range(len(data)):
        ml_type = data[i]['ml_type']
        dict_components[ml_type] = data[i]['component']
        dic_solvers[ml_type] = data[i]['solvers']
        dic_xtypes[ml_type] = data[i]['xtypes']

    return names_models, dict_components, dic_solvers, dic_xtypes
